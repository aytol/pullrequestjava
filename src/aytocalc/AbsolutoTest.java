package aytocalc;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class AbsolutoTest {

	@Test
	void testValorAbosulto() {
		ValorAbsoluto calculadora = new ValorAbsoluto();
		int resultado = calculadora.abs(-10);
		assertEquals(10,resultado);
		
	}
	
	@Test
	void testValorAbosultoPositivo() {
		ValorAbsoluto calculadora = new ValorAbsoluto();
		int resultado = calculadora.abs(10);
		assertEquals(10,resultado);
		
	}


}
